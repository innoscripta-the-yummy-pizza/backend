<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom REST Language Response Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | corresponding the API endpoints.
    |
    */

    'error' => 'error',
    'success' => 'success',

    'session' => [
        'unauthorized_user' => 'Unauthorized user.',
    ],

    'unverified_user' => 'This user is unverified. Please confirm your email before perform this action.',

    'auth' => [
        'register' => [
            'error' => 'An error occurred while triying to create this new customer.',
            'success' => 'Customer was created successfully.',
        ],
        'login' => [
            'error' => 'An error occurred while triying to login.',
            'error_data' => 'There has been an error on the submitted data.',
            'error_attempt' => 'Invalid credentials.',
            'success' => 'Login success.',
        ],
        'logout' => [
            'success' => 'Logout success.',
            'error' => 'Logout error.',
        ],
        'forgot_password' => [
            'success' => 'We have sent you a password recovery email.',
            'error' => 'An error occurred while triying to sent the recover email.'
        ],
        'recover_password' => [
            'parameters_error' => 'There are errors on the provided parameters.',
            'key_not_match' => 'The provided key does not match the right one.',
            'expired_key' => 'Provided key is already expired, please make a new resend recover validation mail.',
            'success' => 'Password restored successfully.',
        ],
        'resend_validation_link' => [
            'success' => 'Validation link has been sent succesfully.',
            'account_already_activated' => 'This account is already activated.',
            'error' => 'An error occurred while sending the validation mail'
        ],
        'validate_account' => [
            'parameters_error' => 'There are errors on the provided parameters.',
            'key_not_match' => 'The provided key does not match the right one.',
            'expired_key' => 'Provided key is already expired, please make a request.',
            'success' => 'Account successfully verified.',
            'already_verified' => 'This account is already verified.',
        ]
    ],

    'menu' => [
        'retrieve_pizzas' => [
            'success' => 'All pizzas retrieve successfully.',
            'error' => 'There was an error while retrieving the pizzas info.'
        ],
        'single_pizza' => [
            'success' => 'Pizza details retrieved successfully.',
            'error' => 'There was an error while getting the pizza detail.',
        ],
        'retrieve_extras' => [
            'success' => 'Extras retrieved successfully.',
            'error' => 'There was an error while retrieving the extras info.',
        ]
    ],

    'profile' => [
        'my_profile' => [
            'success' => 'Profile retrieved succesfully.',
            'error' => 'There was an error while retrieving your profile.',
        ],
        'update_my_profile' => [
            'success' => 'Profile updated successfully.',
            'error' => 'Profile updated successfully.',
            'username_in_use' => 'This username is already being used by another user.',
        ],
        'my_delivery_info' => [
            'success' => 'My delivery info retrieved successfully.',
            'error' => 'There was an error while retrieving my delivery information.',
        ],
        'update_my_delivery_info' => [
            'success' => 'My delivery info was updated successfully',
            'error' => 'There was an error while updating my delivery information.',
        ],
        'update_password' => [
            'success' => 'Password updated succesfully.',
            'error' => 'There was an error while trying to update my password.',
        ],
        'update_profile_picture' => [
            'success' => 'Profile picture updated successfully.',
            'error' => 'There was an error while updating my profile picture.'
        ]
    ],

    'order' => [
        'no_active_order' => 'There is no active orders currently, please add create an order first.',
        'create_order' => [
            'success' => 'Order created successfully.',
            'error' => 'There was an error while creating the order.',
            'order_already_exist' => 'There is already an open order.',
        ],
        'retrieve_current_order' => [
            'success' => 'Current order retrieved successfully.',
            'error' => 'There was an error while getting the current order.'
        ],
        'add_pizza_to_current_order' => [
            'success' => 'Pizza added to the current order successfully',
            'error' => 'The pizza could not be added to the current order. Please try again.'
        ],
        'update_pizza_from_current_order' => [
            'success' => 'Pizza updated successfully on the current order.',
            'error' => 'The pizza could not be updated. Please try again.',
            'no_pizza_order_found' => 'The Pizza Order specified was not found.',
        ],
        'delete_pizza_from_current_order' => [
            'success' => 'Pizza deleted from the current order.',
            'error' => 'The pizza could not be deleted from the current order. Please try again.',
            'no_pizza_order_found' => 'The Pizza Order specified was not found.',
        ],
        'place_order' => [
            'success' => 'Order placed successfully',
            'error' => 'Order could not be placed correctly. Please try again.',
            'order_already_paid' => 'This order was already paid.',
        ],
        'retrieve_actives_orders' => [
            'success' => 'Active orders retrieved successfully.',
            'error' => 'Active orders could not be retrieved. Try again.'
        ],
        'retrieve_history_orders' => [
            'success' => 'History of orders retrieved successfully.',
            'error' => 'There was an error while getting the orders history.'
        ],
        'single_order' => [
            'success' => 'Order retrieved correctly.',
            'error' => 'There was an error while getting the order. Please try again.'
        ],
    ]
];
