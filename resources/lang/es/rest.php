<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom REST Language Response Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | corresponding the API endpoints.
    |
    */

    'error' => 'error',
    'success' => 'success',

    'session' => [
        'not_exist' => 'La sesión del usuario no existe. Intente iniciar sesión nuevamente.',
        'auth_header_missing' => 'La cabecera Authorization es requerida.'
    ],

    'role' => [
        'not_acceptable' => 'Su rol no tiene suficientes privilegios para realizar esta acción.',
        'retrieve' => [
            'success' => 'Los roles han sido recuperados correctamente',
            'error' => 'Ocurrió un error al recuperar los roles '
        ],
        'create' => [
            'success' => 'Rol creado satisfactoriamente.',
            'error' => 'Ocurrió un error durante la creación del rol.',
        ],
        'update' => [
            'success' => 'Rol actualizado satisfactoriamente.',
            'error' => 'Ocurrió un error durante la actualización del Rol.',
            'same_role' => 'No se puede actualizar su propio rol',
            'admin_super_admin_role' => 'El rol de los usuarios Admin o Super Admin no puede ser actualizado.',
        ],
        'delete' => [
            'success' => 'Rol eliminado satisfactoriamente.',
            'error' => 'Ocurrió un error al eliminar el usuario.',
            'same_role' => 'No se puede borrar su propio rol',
            'base_role' => 'Este rol no puede ser eliminado.'
        ]
    ],

    'user' => [
        'login' => [
            'error' => 'Ocurrió un error al intentar iniciar sesión.',
            'success' => 'Sessión iniciada correctamente.',
            'password_not_match' => 'Credenciales inválidas.'
        ],
        'logout' => [
            'success' => 'Sesión finalizada correctamente.',
        ],
        'register' => [
            'error' => 'Ocurrió un error el intentar realizar el registro.'
        ],
        'password_reset' => [
            'success' => 'Te hemos enviado un email para la recuperación de contraseña.',
            'error' => 'Ocurrió un error al intentar enviar el correo de recuperación.'
        ],
        'profile' => [
            'retrieve' => [
                'success' => 'Perfil recuperado éxitosamente.',
                'error' => 'Ocurrió un error al recuperar el perfil.',
            ],
            'update' => [
                'success' => 'Perfil actualizado éxitosamente.',
                'error' => 'Ocurrió un error al actualizar el perfil.',
                'super_admin' => 'El email del usuario (Master) Super Admin no puede ser actualizado.',
            ]
        ],
        'create' => [
            'success' => 'Usuario creado satisfactoriamente.',
            'error' => 'Ocurrió un error durante la creación del usuario.',
        ],
        'retrieve' => [
            'success' => 'Usuarios recuperados satisfactoriamente.',
            'error' => 'Ocurrió un error al recuperar los usuarios.',
        ],
        'single' => [
            'success' => 'Usuario recuperado satisfactoriamente.',
            'error' => 'Ocurrió un error al recuperar el usuario.',
        ],
        'update' => [
            'success' => 'Usuario actualizado satisfactoriamente.',
            'error' => 'Ocurrió un error durante la actualización del usuario.',
            'same_user' => 'Usuario propio no puede ser actualizado',
            'super_admin' => 'El usuario Super Admin no puede ser actualizado.',
            'super_admin_role' => 'El role de los usuarios Super Admin no puede ser actualizado.',
        ],
        'delete' => [
            'success' => 'Usuario eliminado satisfactoriamente.',
            'error' => 'Ocurrió un error al eliminar el usuario.',
            'same_user' => '',
            'super_admin' => '',
        ]
    ],

    'organization' => [
        'retrieve' => [
            'success' => 'Las organizaciones fueron recuperadas satisfactoriamente.',
            'error' => 'Las organizaciones no pudieron ser recuperadas correctamente.'
        ],
        'single' => [
            'success' => 'La organización fue recuperada satisfactoriamente.',
            'error' => 'La organización no pudo ser recuperadas correctamente.'
        ],
        'create' => [
            'success' => 'La organización fue creada satisfactoriamente.',
            'error' => 'La organización no pudo ser creada correctamente.'
        ],
        'update' => [
            'success' => 'La organización fue actualizada satisfactoriamente.',
            'error' => 'La organización no pudo ser actualizada correctamente.'
        ],
        'delete' => [
            'success' => 'La organización fue eliminada satisfactoriamente.',
            'error' => 'La organización no pudo ser eliminada correctamente.'
        ]
    ],
    
    'region' => [
        'retrieve' => [
            'success' => 'Las regiones fueron recuperadas satisfactoriamente.',
            'error' => 'Las regiones no pudieron ser recuperadas correctamente.'
        ],
        'single' => [
            'success' => 'La región fue recuperada satisfactoriamente.',
            'error' => 'La región no pudo ser recuperadas correctamente.'
        ],
        'create' => [
            'success' => 'La región fue creada satisfactoriamente.',
            'error' => 'La región no pudo ser creada correctamente.'
        ],
        'update' => [
            'success' => 'La región fue actualizada satisfactoriamente.',
            'error' => 'La región no pudo ser actualizada correctamente.'
        ],
        'delete' => [
            'success' => 'La región fue eliminada satisfactoriamente.',
            'error' => 'La región no pudo ser eliminada correctamente.'
        ]
    ],
    
    'country' => [
        'retrieve' => [
            'success' => 'Los paises fueron recuperadas satisfactoriamente.',
            'error' => 'Los paises no pudieron ser recuperadas correctamente.'
        ],
        'single' => [
            'success' => 'El país fue recuperada satisfactoriamente.',
            'error' => 'El país no pudo ser recuperadas correctamente.'
        ],
        'create' => [
            'success' => 'El país fue creada satisfactoriamente.',
            'error' => 'El país no pudo ser creada correctamente.'
        ],
        'update' => [
            'success' => 'El país fue actualizada satisfactoriamente.',
            'error' => 'El país no pudo ser actualizada correctamente.'
        ],
        'delete' => [
            'success' => 'El país fue eliminada satisfactoriamente.',
            'error' => 'El país no pudo ser eliminada correctamente.'
        ]
    ],

    'city' => [
        'retrieve' => [
            'success' => 'Las ciudades fueron recuperadas satisfactoriamente.',
            'error' => 'Las ciudades no pudieron ser recuperadas correctamente.'
        ],
        'single' => [
            'success' => 'La ciudad fue recuperada satisfactoriamente.',
            'error' => 'La ciudad no pudo ser recuperadas correctamente.'
        ],
        'create' => [
            'success' => 'La ciudad fue creada satisfactoriamente.',
            'error' => 'La ciudad no pudo ser creada correctamente.'
        ],
        'update' => [
            'success' => 'La ciudad fue actualizada satisfactoriamente.',
            'error' => 'La ciudad no pudo ser actualizada correctamente.'
        ],
        'delete' => [
            'success' => 'La ciudad fue eliminada satisfactoriamente.',
            'error' => 'La ciudad no pudo ser eliminada correctamente.'
        ]
    ],

    'news' => [
        'retrieve' => [
            'success' => 'Los posts de noticias fueron recuperados satisfactoriamente',
            'error' => 'Los posts de noticias no pudieron ser recuperados correctamente.'
        ],
        'create' => [
            'success' => 'La noticia fue creada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar crear la noticia.',
        ],
        'update' => [
            'success' => 'La noticia fue actualizada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar actualizar la noticia.',
        ],
        'delete' => [
            'success' => 'La noticia fue eliminada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar eliminar la noticia.',
        ],
        'share' => [
            'success' => 'El link de compartir noticia fue obtenido satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar crear el link para compartir noticia.',
        ]
    ],

    'news_categories' => [
        'retrieve' => [
            'success' => 'Las categorías fueron recuperadas satisfactoriamente.',
        ],
    ],

    'news_resources' => [
        'create' => [
            'success' => 'El recurso fue creado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar crear el recurso.',
        ],
    ],

    'settings' => [
        'retrieve' => [
            'success' => 'Las configuraciones fueron recuperadas sactisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar las configuraciones.',
        ],
        'single' => [
            'success' => 'La configuración fue recuperada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar la configuración.'
        ],
        'update' => [
            'success' => 'La configuración fue actualizada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar actualizar la configuración.'
        ]
    ],

    'forum_categories' => [
        'retrieve' => [
            'success' => 'Las categorías del foro fueron recuperadas satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar las categorías del foro.'
        ],
        'single' => [
            'success' => 'La categoría del foro fue recuperada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar la categoría del foro.'
        ],
        'create' => [
            'success' => 'La categoría del foro fue creada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar crear la categoría del foro.',
        ],
        'update' => [
            'success' => 'La categoría del foro fue actualizada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar actualizar la categoría del foro.',
        ],
        'delete' => [
            'success' => 'La categoría del foro fue eliminada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar eliminar la categoría del foro.',
        ]
    ],

    'posts' => [
        'retrieve' => [
            'success' => 'Los posts del foro fueron recuperadas satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar los posts del foro.'
        ],
        'create' => [
            'success' => 'El post fue creado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar crear el post.',
        ],
        'update' => [
            'success' => 'El post fue actualizado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar actualizar el post.',
            'not_user' => 'El usuario haciendo la solicitud no es el autor, no puede actualizar el post.'
        ],
        'delete' => [
            'success' => 'El post fue eliminado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar eliminar el post.',
            'not_user' => 'El usuario haciendo la solicitud no es el autor, no puede eliminar el comentario.'
        ]

    ],

    'comments' => [
        'create' => [
            'success' => 'El comentario fue creado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar crear el comentario.'
        ],
        'update' => [
            'success' => 'El comentario fue actualizado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar actualizar el comentario.',
            'not_user' => 'El usuario haciendo la solicitud no es el autor, no puede actualizar el comentario.'
        ],
        'delete' => [
            'success' => 'El comentario fue eliminado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar eliminar el comentario.',
            'not_user' => 'El usuario haciendo la solicitud no es el autor, no puede borrar el comentario'
        ],
    ],

    'sections' => [
        'retrieve' => [
            'success' => 'Las secciones fueron recuperadas satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar las secciones.'
        ],
    ],

    'notifications' => [
        'retrieve' => [
            'success' => 'Las notificaciones fueron recuperadas satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar las notificaciones.'
        ],
        'single' => [
            'success' => 'La notificación fue recuperada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar la notificación.'
        ],
        'user_unread' => [
            'success' => 'Las notificaciones no leidas del usuario fueron recuperadas satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar las notificaciones no leidas del usuario.'
        ],
        'user_all' => [
            'success' => 'Las notificaciones del usuario fueron recuperadas satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar las notificaciones del usuario.'
        ],
        'read' => [
            'success' => 'La notificacion fue marcada como leida',
            'error' => 'Ocurrió un error al intentar marcar la notificacion como leida.'
        ],
        'user_all' => [
            'success' => 'La notificacion fue marcada como no leida',
            'error' => 'Ocurrió un error al intentar marcar la notificacion como no leida.'
        ],
        'update_token' => [
            'success' => 'El token Firebase Cloud Messaging fue actualizado correctamente.',
            'error' => 'El token Firebase Cloud Messaging no pudo ser actualizado.',
        ],
        'create' => [
            'success' => 'La notificación fue enviada satisfactoriamente.',
            'error' => 'Hubo un error al enviar la notificación.'
        ]
    ],
    'subscriptions' => [
        'plans' => [
            'retrieve' => [
                'success' => 'Los planes de suscripcion fueron recuperados satisfactoriamente.',
                'error' => 'Ocurrió un error al intentar recuperar los planes de suscripcion.'
            ],
            'single' => [
                'success' => 'El plan de suscripcion fue recuperado satisfactoriamente.',
                'error' => 'Ocurrió un error al intentar recuperar el plan de suscripcion.'
            ],
            'create' => [
                'success' => 'El plan fue creado satisfactoriamente.',
                'error' => 'Ocurrió un error al intentar crear el plan.'
            ],
            'update' => [
                'success' => 'El plan fue actualizado satisfactoriamente.',
                'error' => 'Ocurrió un error al intentar actualizar el plan.'
            ],
            'delete' => [
                'success' => 'El plan fue eliminado satisfactoriamente.',
                'error' => 'Ocurrió un error al intentar eliminar el plan.'
            ],         
        ],
        'create' => [
            'success' => 'La suscripcion fue creada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar crear la suscripcion.'
        ],
        'change' => [
            'success' => 'La suscripcion fue actualizada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar actualizar la suscripcion.'
        ],
        'delete' => [
            'success' => 'La suscripcion fue eliminada satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar eliminar la suscripcion.'
        ],
        'redeem' => [
            'success' => 'El codigo de suscripcion fue reclamado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar reclamar el codigo de suscripcion.',
            'no_spots_available' => 'El numero maximo de reclamos para este codigo ha sido alcanzado.'
        ],  
        'release' => [
            'success' => 'El codigo de suscripcion fue liberado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar liberar el codigo de suscripcion.',
        ],  
        'user_has_subscription' => 'El usuario ya se encuentra suscrito a un plan',
        'user_not_subscribed' => 'El usuario no tiene suscripcion activa',
        'user_not_owner' => 'El usuario no fue el creador de esta subscripcion, no puede borrarla.',
        'price_not_belong_to_plan' => 'El precio no pertenece al plan selecionado.',
        'invoices' => [
            'success' => 'Facturas recuperadas correctamente.',
            'error' => 'Hubo un error al recuperar las facturas.'
        ]
    ],

    'training_packages' => [
        'retrieve' => [
            'success' => 'Los paquetes de entrenamiento fueron recuperados satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar los paquetes de entrenamiento.'
        ],
        'delete' => [
            'success' => 'El paquete de entrenamiento fue eliminado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar eliminar el paquete de entrenamiento.',
        ] 
    ],

    'dashboard' => [
        'retrieve' => [
            'success' => 'Los puntajes del usuario fueron recuperados satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar los puntajes de usuario.'
        ], 
    ],

    'object_features' => [
        'retrieve' => [
            'success' => 'Las propiedades de objetos fueron recuperados satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar las propiedades de objetos.'
        ],    
    ],

    'stripe' => [
        'subscriptions' => [
            'create' => [
                'success' => 'La suscripción de Stripe fue creada satisfactoriamente.',
                'error' => 'Ocurrió un error al crear la suscripción de stripe.',
            ]
        ]
    ],

    'object' => [
        'retrieve' => [
            'success' => 'Los objetos fueron recuperados satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar los objetos.'
        ],
        'single' => [
            'success' => 'El objeto fue recuperado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar el objeto.'
        ]
    ],

    'environment' => [
        'retrieve' => [
            'success' => 'Los entornos fueron recuperados satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar los entornos.'
        ],
        'single' => [
            'success' => 'El entorno fue recuperado satisfactoriamente.',
            'error' => 'Ocurrió un error al intentar recuperar el entorno.'
        ]
    ]
];
