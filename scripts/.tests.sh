# Running test using sqlite database and a secondary environment
composer dump-autoload
rm ./tests/database.sqlite
php artisan config:cache --env=testing
php artisan cache:clear --env=testing
touch ./tests/database.sqlite
php artisan migrate:fresh --seed --env=testing
php artisan passport:keys --force --env=testing
php artisan passport:client --personal --name="TheYummyPizza Password Grant Client Customer" --provider=customers --env=testing
php artisan passport:client --personal --name="TheYummyPizza Password Grant Client Admin" --provider=admins --env=testing
php artisan test --env=testing

# Clean current environment
php artisan config:cache
php artisan cache:clear
