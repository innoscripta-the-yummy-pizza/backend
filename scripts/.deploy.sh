composer dump-autoload
php artisan cache:clear
php artisan config:cache
php artisan migrate --force
pm2 flush
pm2 reload ecosystem.config.js
sudo service nginx restart
