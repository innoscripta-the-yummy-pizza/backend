#!/bin/bash
echo "Running delivery"

# Get the Load Balancer instances
instance_ids=`aws elbv2 describe-target-health --target-group-arn $AWS_ELB_TARGET_ARN --query 'TargetHealthDescriptions[*].Target.Id' --output text`

# Deploy on each instance
for instance_id in  $(echo $instance_ids | tr " " "\n")
do
    server_ip=`aws ec2 describe-instances --instance-ids ${instance_id} --query 'Reservations[*].Instances[*].PublicIpAddress' --output text`
    scp -r .env ${CI_ENVIRONMENT_NAME}@${server_ip}:~/api
    scp -r ./* ${CI_ENVIRONMENT_NAME}@${server_ip}:~/api
    ssh ${CI_ENVIRONMENT_NAME}@${server_ip} "cd api/ && bash scripts/.deploy.sh"
done
