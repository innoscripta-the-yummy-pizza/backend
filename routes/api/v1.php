<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
if (App::environment('production') || App::environment('staging') || App::environment('develop')) {
    URL::forceScheme('https');
}

Route::group([ 'as' => 'api.v1.public.' ], function () {
    Route::prefix('auth')->group(function() {
        Route::post('/sign-up','AuthController@signUp');
        Route::post('/sign-in','AuthController@signIn');
        Route::post('/forgot-password','AuthController@forgotPassword');
        Route::post('/recover-password/{id}/{key}','AuthController@recoverPassword');
        Route::post('/validate-account/{id}/{key}','AuthController@validateAccount');
    });

    Route::prefix('menu')->group(function() {
        Route::get('/', 'MenuController@retrievePizzas');
        Route::get('/extras', 'MenuController@retrieveExtras');
        Route::get('/{pizza_id}', 'MenuController@singlePizza');
    });
});

Route::middleware('auth:api:customer')->group(function () {
    Route::prefix('auth')->group(function() {
        Route::post('/resend-validation-link','AuthController@resendValidationLink');
    });

    /*Route::prefix('menu')->group(function() {
        Route::get('/extras', 'MenuController@retrieveExtras');
        Route::get('/{pizza_id}', 'MenuController@singlePizza');
    });*/

    Route::prefix('profile')->group(function() {
        Route::get('/', 'ProfileController@myProfile');
        Route::patch('/', 'ProfileController@updateMyProfile');
        Route::get('/delivery', 'ProfileController@myDeliveryInfo');
        Route::patch('/delivery', 'ProfileController@updateMyDeliveryInfo');
        Route::put('/update-password', 'ProfileController@updatePassword');
        Route::post('/update-profile-picture', 'ProfileController@updateProfilePicture');
    });

    Route::prefix('order')->group(function() {
        Route::post('/', 'OrderController@createOrder');
        Route::get('/', 'OrderController@retrieveCurrentOrder');
        Route::post('/pizza/{pizza_id}', 'OrderController@addPizzaToCurrentOrder');
        Route::put('/pizza/{pizza_order_id}', 'OrderController@updatePizzaFromCurrentOrder');
        Route::delete('/pizza/{pizza_order_id}', 'OrderController@deletePizzaFromCurrentOrder');
        Route::post('/place', 'OrderController@placeOrder');
        Route::get('/actives', 'OrderController@retrieveActiveOrders');
        Route::get('/history', 'OrderController@retrieveHistoryOrders');
        Route::get('/{order_id}', 'OrderController@singleOrder');
    });
});
