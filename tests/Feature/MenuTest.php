<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class MenuTest extends TestCase
{
    use WithFaker;

    public function testRetrievePizzas(){
        $response = $this->json('GET', '/api/v1/menu' );

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => 'success',
            'statusCode' => 200
        ]);
    }
}
