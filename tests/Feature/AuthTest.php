<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use WithFaker;

    /**
     * Register customer
     */
    public function testRegisterCustomer()
    {
        $firstName = $this->faker->firstName();
        $username = slugify($firstName);
        $password = md5(rand());
        $data = [
            'username' => $username,
            'email' => $this->faker->email(),
            'first_name' => $firstName,
            'last_name' => $this->faker->lastName(),
            'password' => $password,
            'password_confirmation' => $password
        ];

        // Email nor valid case
        $data['email'] = $this->faker->firstName();
        $response = $this->json('POST', '/api/v1/auth/sign-up', $data);
        $response
            ->assertStatus(400)
            ->assertJson([
                'status' => 'error',
                'statusCode' => 400
            ]);

        // Missing field
        unset($data['username']);
        $response = $this->json('POST', '/api/v1/auth/sign-up', $data);
        $response
            ->assertStatus(400)
            ->assertJson([
                'status' => 'error',
                'statusCode' => 400
            ]);

        // Success case
        $data['email'] = $this->faker->email();
        $data['username'] = $username;
        $response = $this->json('POST', '/api/v1/auth/sign-up', $data);
        $response
            ->assertStatus(201)
            ->assertJson([
                'status' => 'success',
                'statusCode' => 201
            ]);
    }

    /**
     * Login customer
     */
    public function testLoginCustomer()
    {
        $email = 'test@example.com';
        $password = 'testuser123456';

        // Missing field
        $response = $this->json('POST', '/api/v1/auth/sign-in', [
            'password' => $password
        ]);
        $response
            ->assertStatus(400)
            ->assertJson([
                'status' => 'error',
                'statusCode' => 400
            ]);

        // Invalid data
        $response = $this->json('POST', '/api/v1/auth/sign-in', [
            'email' => 'invalidmail',
            'password' => $password,
        ]);
        $response
            ->assertStatus(400)
            ->assertJson([
                'status' => 'error',
                'statusCode' => 400
            ]);

        // Invalid info
        $response = $this->json('POST', '/api/v1/auth/sign-in', [
            'email' => 'other@example.com',
            'password' => $password,
        ]);
        $response
            ->assertStatus(400)
            ->assertJson([
                'status' => 'error',
                'statusCode' => 400
            ]);

        // Success case
        $response = $this->json('POST', '/api/v1/auth/sign-in', [
            'email' => $email,
            'password' => $password,
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success',
                'statusCode' => 200
            ]);
    }

    /**
     * Forgot Password
     */
    public function testForgotPassword(){
        $email = 'test@example.com';

        $response = $this->json('POST', '/api/v1/auth/forgot-password', [
            'email' => $email
        ]);

        $response
        ->assertStatus(200)
        ->assertJson([
            'status' => 'success',
            'statusCode' => 200
        ]);

        $response = $this->json('POST', '/api/v1/auth/forgot-password', [
            'email' => "other@example.com"
        ]);
        $response
        ->assertStatus(400)
        ->assertJson([
            'status' => 'error',
            'statusCode' => 400
        ]);
    }
}
