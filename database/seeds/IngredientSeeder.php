<?php

use Illuminate\Database\Seeder;
use Models\Ingredient;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createIngredient('Corn', 5.21);
        $this->createIngredient('Peppers', 6.21);
        $this->createIngredient('Mozzarella cheese', 2.21);
        $this->createIngredient('Parmesan cheese', 2.99);
        $this->createIngredient('Spanish chorizo', 3.66);
        $this->createIngredient('Onion', 1.55);
        $this->createIngredient('Mushrooms', 7.99);
        $this->createIngredient('Cheddar cheese', 1.99);
        $this->createIngredient('Chicken', 9.21);
        $this->createIngredient('Pepperoni', 2.55);
        $this->createIngredient('Tomato', 1.23);
        $this->createIngredient('Ham', 2.43);
        $this->createIngredient('Bacon', 3.65);
        $this->createIngredient('Serrano ham', 9.99);
    }

    public function createIngredient($name, $price) {
        try {
            // Creates a ingredient if not exist
            $data = [
                'name' => $name,
                'price' => $price,
            ];
            $ingredient = Ingredient::select()
                ->where($data)
                ->first();

            if (!isset($ingredient)) {
                $ingredient = Ingredient::create($data);
            }
        } catch (\Exception $exception) {
            logger($exception->getMessage());
        }
    }
}
