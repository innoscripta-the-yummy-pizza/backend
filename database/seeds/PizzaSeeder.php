<?php

use Illuminate\Database\Seeder;
use Models\Pizza;

class PizzaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createPizza('Bacon Corn', 'Tender corn and bacon.', 'BaconCorn.jpg', 19.99);
        $this->createPizza('Bacon Corn 2', 'Tender corn and extra bacon.', 'BaconCorn2.jpg', 21.99);
        $this->createPizza('Chicken And Mustard Honey', 'Cheese and honey mustard chicken.', 'ChickenAndMustardHoney.jpg', 24.99);
        $this->createPizza('Chicken BBQ', 'Chicken in BBQ sauce.', 'ChickenBBQ.jpg', 24.99);
        $this->createPizza('Creole', 'Tender corn and shredded meat.', 'Creole.jpg', 22.99);
        $this->createPizza('Creole 2', 'Cheese, shredded meat and young corn.', 'Creole2.jpg', 22.99);
        $this->createPizza('Hawaiian', 'Cheese, ham and pineapple.', 'Hawaiian.jpg', 22.99);
        $this->createPizza('Join Us Yummy', 'Mozzarella cheese, pepperoni, ham, onion, green paprika, mushrooms and black olives.', 'JoinUsMaxima.jpg', 25.99);
        $this->createPizza('Meats', 'Cheese, ham, salami and pepperoni.', 'Meats.jpg', 21.99);
        $this->createPizza('Mexican', 'Pico de gallo, ground beef and jalapeños.', 'Mexican.jpg', 23.99);
        $this->createPizza('Mexican 2', 'Ground beef, paprika, onion, cheese, tostacos and jalapeños.', 'Mexican2.jpg', 23.99);
        $this->createPizza('Mixed', 'Cheese, chicken and meat.', 'Mixed.jpg', 20.24);
        $this->createPizza('Neapolitan', 'Cheese, tomato and oregano.', 'Neapolitan.jpg', 19.99);
        $this->createPizza('Chicken with Mushrooms', 'Chicken, mushrooms and cheese.', 'ChickenMushrooms.jpg', 22.99);
        $this->createPizza('Spanish', 'Pepper, pepperoni, choricillo, ham and tomato.', 'Spanish.jpg', 22.99);
        $this->createPizza('Special Meat', 'American pepperoni, caban, ham and bacon.', 'SpecialMeat.jpg', 23.99);
        $this->createPizza('Tropical', 'Cheese, pineapple, cherries, plum, and peaches.', 'Tropical.jpg', 25.99);
        $this->createPizza('Vegeratian', 'Cheese, mushrooms, bell peppers, tomato, young corn and coriander.', 'Vegeratian.jpg', 20.00);
    }

    public function createPizza($name, $description, $image, $price) {
        try {
            // Creates a pizza if not exist
            $data = [
                'name' => $name,
                'description' => $description,
                'image' => $image,
                'price' => $price,
            ];
            $pizza = Pizza::select()
                ->where($data)
                ->first();

            if (!isset($pizza)) {
                $pizza = Pizza::create($data);
            }
        } catch (\Exception $exception) {
            logger($exception->getMessage());
        }
    }
}
