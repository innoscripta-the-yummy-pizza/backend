<?php

use Illuminate\Database\Seeder;
use Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            // Creates superadmin user if not exists
            $superadmin = Admin::select()
                ->where([
                    'email' => config('app.superadmin.email'),
                    'username' => config('app.superadmin.username'),
                ])
                ->first();

            if (!isset($superadmin)) {
                $superadmin = Admin::create([
                    'email' => config('app.superadmin.email'),
                    'username' => config('app.superadmin.username'),
                    'password' => bcrypt(config('app.superadmin.password')),
                ]);
            }
        } catch (\Exception $exception) {
            logger($exception->getMessage());
        }
    }
}
