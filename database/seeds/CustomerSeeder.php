<?php

use Illuminate\Database\Seeder;
use Models\Customer;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            // Creates seed user if not exists
            $customer = Customer::select()
                ->where([
                    'email' => 'test@example.com',
                    'username' => 'testuser',
                ])
                ->first();

            if (!isset($customer)) {
                $customer = Customer::create([
                    'email' => 'test@example.com',
                    'first_name' => 'Test',
                    'last_name' => 'User',
                    'username' => 'testuser',
                    'password' => bcrypt('testuser123456'),
                ]);
            }
        } catch (\Exception $exception) {
            logger($exception->getMessage());
        }
    }
}
