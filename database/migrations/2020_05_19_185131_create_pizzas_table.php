<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePizzasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('pizzas');
        Schema::create('pizzas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 255)->nullable(false);
            $table->string('image', 255)->nullable(false);
            $table->text('description')->nullable(false);
            $table->float('price')->nullable(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pizzas');
    }
}
