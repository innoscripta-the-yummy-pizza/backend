<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePizzaOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('pizza_orders');
        Schema::create('pizza_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('quantity')->default(1)->nullable(false);
            $table->integer('pizza_id')->unsigned()->nullable(true);
            $table->integer('order_id')->unsigned()->nullable(true);

            $table->timestamps();

            // Foreign keys
            $table->foreign('pizza_id')->references('id')->on('pizzas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pizza_orders');
    }
}
