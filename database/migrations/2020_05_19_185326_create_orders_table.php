<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('orders');
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('customer_id')->unsigned()->nullable(true);
            $table->enum('status', ['created', 'preparing', 'delivery', 'completed', 'delayed', 'canceled'])->default('created')->nullable(false);
            $table->float('total_price')->nullable(false);
            $table->integer('estimated_delivery_time')->default(15)->nullable(false);
            $table->float('delivery_cost')->default(15.0)->nullable(false);
            $table->boolean('paid')->default(false)->nullable(false);

            $table->timestamps();

            // Foreign keys
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
