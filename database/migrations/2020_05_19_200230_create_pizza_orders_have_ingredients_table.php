<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePizzaOrdersHaveIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('pizza_orders_have_ingredients');
        Schema::create('pizza_orders_have_ingredients', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pizza_order_id')->unsigned()->nullable(true);
            $table->integer('ingredient_id')->unsigned()->nullable(true);

            $table->timestamps();

            // Foreign keys
            $table->foreign('pizza_order_id')->references('id')->on('pizza_orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ingredient_id')->references('id')->on('ingredients')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pizza_orders_have_ingredients');
    }
}
