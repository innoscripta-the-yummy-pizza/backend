<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('customers');
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');

            // Basic information
            $table->string('username', 50)->unique()->nullable(false);
            $table->string('first_name', 50)->nullable(false);
            $table->string('last_name', 50)->nullable();
            $table->string('email')->unique()->nullable(false);
            $table->string('password')->nullable(false);
            $table->string('profile_picture')->nullable(true);

            // Address information | Contact information
            $table->text('address_line_1')->nullable();
            $table->text('address_line_2')->nullable();
            $table->string('city', 50)->nullable();
            $table->string('zip', 20)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('secondary_phone', 20)->nullable();

            // Key validation info
            $table->timestamp('verified_at')->nullable();
            $table->boolean('verified')->default(false);
            $table->string('key', 255)->default(md5(rand()))->nullable();
            $table->timestamp('key_expiration')->default(now()->addDays(1))->nullable();

            // Reset password
            $table->string('forgot_password_key', 255)->default(md5(rand()))->nullable();
            $table->timestamp('forgot_password_key_expiration')->default(now()->addDays(1))->nullable();

            // General information
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
