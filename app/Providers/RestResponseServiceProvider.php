<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\JsonResponse;

class RestResponseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register the application's response macros.
     *
     * @return void
     *
     */


    /**
     * @apiDefine StatusSuccess
     * @apiSuccess {String} status Response general status [success, error].
     */
    /**
     * @apiDefine StatusError
     * @apiError {String} status Response general status [success, error].
     */
    /**
     * @apiDefine StatusCodeSuccess
     * @apiSuccess {Number} statusCode Response http status code.
     */
    /**
     * @apiDefine StatusCodeError
     * @apiError {Number} statusCode Response http status code.
     */
    /**
     * @apiDefine MessageSuccess
     * @apiSuccess {String} message Common information description message.
     */
    /**
     * @apiDefine MessageError
     * @apiError {String} message Common information description message.
     */
    /**
     * @apiDefine ErrorsSuccess
     * @apiSuccess {Object} errors Common errors description.
     */
    /**
     * @apiDefine ErrorsError
     * @apiError {Object} errors Common errors description.
     */
    public function boot()
    {
        Response::macro('restResponse', function ($restResponse) {
            //-- Validations
            if ( !isset($restResponse['status']) ) throw new \Exception("{status} must be included in this REST response configuration");
            if ( !isset($restResponse['statusCode']) ) throw new \Exception("{statusCode} must be included in this REST response configuration");
            if ( !isset($restResponse['message']) ) throw new \Exception("{message} must be included in this REST response configuration");

            //-- Response
            $rest = (object) $restResponse;
            return JsonResponse::create([
                "status" => $rest->status,
                "statusCode" => $rest->statusCode,
                "message" => $rest->message,
                "data" => isset($restResponse['data']) ? $rest->data : NULL,
                "errors" => isset($restResponse['errors']) ? $rest->errors : NULL,
            ], $rest->statusCode);
        });
    }
}
