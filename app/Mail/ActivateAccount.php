<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ActivateAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Activate your The Yummy Pizza account!';
        return $this->view('emails.activate_account')
            ->subject($subject)
            ->with([
                'id' => $this->data['id'],
                'name' => $this->data['name'],
                'username' => $this->data['username'],
                'email' => $this->data['email'],
                'key' => $this->data['key'],
            ]);
    }
}
