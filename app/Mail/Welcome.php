<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Welcome to The Yummy Pizza!';
        return $this->view('emails.welcome')
            ->subject($subject)
            ->with([
                'id' => $this->data['id'],
                'name' => $this->data['name'],
                'username' => $this->data['username'],
                'email' => $this->data['email'],
                'key' => $this->data['key'],
            ]);
    }
}
