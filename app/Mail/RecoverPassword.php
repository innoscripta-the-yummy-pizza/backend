<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecoverPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Recover The Yummy Pizza Account!';
        return $this->view('emails.recover_password')
            ->subject($subject)
            ->with([
                'id' => $this->data['id'],
                'name' => $this->data['name'],
                'forgot_password_key' => $this->data['forgot_password_key'],
            ]);
    }
}
