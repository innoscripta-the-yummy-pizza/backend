<?php
/**
 * Helper custom functions file
 * Write here what you need global
 */

function httpStatusCode ($code) {
	if (!isset($code)) return 500;
	switch ($code) {
		/**
		 * @apiDefine HttpContinue
		 * @apiSuccessExample Continue:
		 * HTTP/1.1 100 Continue
		 * {
		 *      "status": "success",
		 *      "statusCode": 100,
		 *      "message": "Continue",
		 *      "data": NULL,
		 *      "errors": NULL
		 * }
		 */
		case 100: $text = 'Continue'; return 100;


		/**
		 * @apiDefine HttpSwitchingProtocols
		 * @apiSuccessExample Switching Protocols:
		 * HTTP/1.1 101 Switching Protocols
		 * {
		 *      "status": "success",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 101: $text = 'Switching Protocols'; return 101;


		/**
		 * @apiDefine HttpOK
		 * @apiSuccessExample OK:
		 * HTTP/1.1 200 OK
		 * {
		 *      "status": "success",
		 *      "statusCode": 200,
		 *      "message": "OK",
		 *      "data": NULL,
		 *      "errors": NULL
		 * }
		 */
		case 200: $text = 'OK'; return 200;


		/**
		 * @apiDefine HttpCreated
		 * @apiSuccessExample Created:
		 * HTTP/1.1 201 Created
		 * {
		 *      "status": "success",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 201: $text = 'Created'; return 201;


		/**
		 * @apiDefine HttpAccepted
		 * @apiSuccessExample Accepted:
		 * HTTP/1.1 202 Accepted
		 * {
		 *      "status": "success",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 202: $text = 'Accepted'; return 202;


		/**
		 * @apiDefine HttpNonAuthoritativeInformation
		 * @apiSuccessExample Non-Authoritative Information:
		 * HTTP/1.1 203 Non-Authoritative Information
		 * {
		 *      "status": "success",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 203: $text = 'Non-Authoritative Information'; return 203;


		/**
		 * @apiDefine HttpNoContent
		 * @apiSuccessExample No Content:
		 * HTTP/1.1 204 No Content
		 * {
		 *      "status": "success",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 204: $text = 'No Content'; return 204;


		/**
		 * @apiDefine HttpResetContent
		 * @apiSuccessExample Reset Content:
		 * HTTP/1.1 205 Reset Content
		 * {
		 *      "status": "success",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 205: $text = 'Reset Content'; return 205;


		/**
		 * @apiDefine HttpPartialContent
		 * @apiSuccessExample Partial Content:
		 * HTTP/1.1 206 Partial Content
		 * {
		 *      "status": "success",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 206: $text = 'Partial Content'; return 206;


		/**
		 * @apiDefine HttpMultipleChoices
		 * @apiErrorExample Multiple Choices:
		 * HTTP/1.1 300 Multiple Choices
		 * {
		 *      "status": "error",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 300: $text = 'Multiple Choices'; return 300;


		/**
		 * @apiDefine HttpMovedPermanently
		 * @apiErrorExample Moved Permanently:
		 * HTTP/1.1 301 Moved Permanently
		 * {
		 *      "status": "error",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 301: $text = 'Moved Permanently'; return 301;


		/**
		 * @apiDefine HttpMovedTemporarily
		 * @apiErrorExample Moved Temporarily:
		 * HTTP/1.1 302 Moved Temporarily
		 * {
		 *      "status": "error",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 302: $text = 'Moved Temporarily'; return 302;


		/**
		 * @apiDefine HttpSeeOther
		 * @apiErrorExample See Other:
		 * HTTP/1.1 303 See Other
		 * {
		 *      "status": "error",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 303: $text = 'See Other'; return 303;


		/**
		 * @apiDefine HttpNotModified
		 * @apiErrorExample Not Modified:
		 * HTTP/1.1 304 Not Modified
		 * {
		 *      "status": "error",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 304: $text = 'Not Modified'; return 304;


		/**
		 * @apiDefine HttpUseProxy
		 * @apiErrorExample Use Proxy:
		 * HTTP/1.1 305 User Proxy
		 * {
		 *      "status": "error",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 305: $text = 'Use Proxy'; return 305;


		/**
		 * @apiDefine HttpBadRequest
		 * @apiErrorExample Bad Request:
		 * HTTP/1.1 400 Bad Request
		 * {
		 *     "status": "error",
		 *     "statusCode": 400,
		 *     "message": "Specific Backend Error",
		 *     "data": null,
		 *     "errors": List Of Errors
		 * }
		 */
		case 400: $text = 'Bad Request'; return 400;


		/**
		 * @apiDefine HttpUnauthorized
		 * @apiErrorExample Unauthorized:
		 * HTTP/1.1 401 Unauthorized
		 * {
		 *     "status": "error",
		 *     "statusCode": 401,
		 *     "message": "Specific Backend Error",
		 *     "data": null,
		 *     "errors": null
		 * }
		 */
		case 401: $text = 'Unauthorized'; return 401;


		case 402: $text = 'Payment Required'; return 402;

		/**
		 * @apiDefine HttpBadForbidden
		 * @apiErrorExample Forbidden:
		 * HTTP/1.1 403 Forbidden
		 * {
		 *     "status": "error",
		 *     "statusCode": 403,
		 *     "message": "Specific Backend Error",
		 *     "data": null,
		 *     "errors": List Of Errors
		 * }
		 */
		case 403: $text = 'Forbidden'; return 403;


		/**
		 * @apiDefine HttpNotFound
		 * @apiErrorExample Not Found:
		 * HTTP/1.1 404 Not Found
		 * {
		 *      "status": "error",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 404: $text = 'Not Found'; return 404;


		/**
		 * @apiDefine HttpMethodNotAllowed
		 * @apiErrorExample Method Not Allowed:
		 * HTTP/1.1 405 Method Not Allowed
		 * {
		 *      "status": "error",
		 *      "data": "Descriptive message"
		 * }
		 */
		case 405: $text = 'Method Not Allowed'; return 405;

		/**
		 * @apiDefine HttpNotAcceptable
		 * @apiErrorExample Not Acceptable:
		 * HTTP/1.1 406 Not Acceptable
		 * {
		 *     "status": "error",
		 *     "statusCode": 406,
		 *     "message": "Specific Backend Error",
		 *     "data": null,
		 *     "errors": null
		 * }
		 */
		case 406: $text = 'Not Acceptable'; return 406;
		case 407: $text = 'Proxy Authentication Required'; return 407;
		case 408: $text = 'Request Time-out'; return 408;
		case 409: $text = 'Conflict'; return 409;
		case 410: $text = 'Gone'; return 410;
		case 411: $text = 'Length Required'; return 411;
		case 412: $text = 'Precondition Failed'; return 412;
		case 413: $text = 'Request Entity Too Large'; return 413;
		case 414: $text = 'Request-URI Too Large'; return 414;
		case 415: $text = 'Unsupported Media Type'; return 415;


		/**
		 * @apiDefine HttpInternalServerError
		 * @apiErrorExample Internal Server Error:
		 * HTTP/1.1 500 Internal Server Error
		 * {
		 *     "status": "error",
		 *     "statusCode": 500,
		 *     "message": "Specific Backend Error",
		 *     "data": null,
		 *     "errors": null
		 * }
		 */
		case 500: $text = 'Internal Server Error'; return 500;
		case 501: $text = 'Not Implemented'; return 501;
		case 502: $text = 'Bad Gateway'; return 502;
		case 503: $text = 'Service Unavailable'; return 503;
		case 504: $text = 'Gateway Time-out'; return 504;
		case 505: $text = 'HTTP Version not supported'; return 505;
		default: return 500;
	}
}


function slugify($string) {
    return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
}
