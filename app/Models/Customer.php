<?php

namespace Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Laravel\Cashier\Billable;

class Customer extends Authenticatable
{
    use HasApiTokens, Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'first_name',
        'last_name',
        'email',
        'password',
        'address_line_1',
        'address_line_2',
        'city',
        'zip',
        'phone',
        'secondary_phone',
        'verified',
        'key',
        'key_expiration',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'key', 'key_expiration'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'verified_at' => 'datetime',
        'key_expiration' => 'datetime',
    ];

    protected $appends = [
        'profile_picture_url'
    ];

    function orders() {
        return $this->hasMany(Order::class, 'customer_id', 'id')
            ->with([
                'pizza_orders',
                'pizza_orders.pizza',
                'pizza_orders.extras',
            ]);
    }

    function active_orders() {
        return $this->hasMany(Order::class, 'customer_id', 'id')
            ->where([ 'status' => 'preparing' ])
            ->orWhere([ 'status' => 'delivery' ])
            ->orWhere([ 'status' => 'delayed' ])
            ->with([
                'pizza_orders',
                'pizza_orders.pizza',
                'pizza_orders.extras',
            ]);
    }

    function history_orders() {
        return $this->hasMany(Order::class, 'customer_id', 'id')
            ->where([ 'status' => 'completed' ])
            ->orWhere([ 'status' => 'canceled' ])
            ->with([
                'pizza_orders',
                'pizza_orders.pizza',
                'pizza_orders.extras',
            ]);
    }

    function current_order() {
        return $this->hasOne(Order::class, 'customer_id', 'id')
            ->where([ 'status' => 'created' ])
            ->with([
                'pizza_orders',
                'pizza_orders.pizza',
                'pizza_orders.extras',
            ]);
    }

    /**
     * Append attributes
     */
    public function getProfilePictureUrlAttribute()
    {
        $profile_picture = isset($this->attributes['profile_picture']) ? $this->attributes['profile_picture'] : NULL;
        return isset($profile_picture) ? config('app.resources_url') . "/profile/$profile_picture" : NULL;
    }
}
