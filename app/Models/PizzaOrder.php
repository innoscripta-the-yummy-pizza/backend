<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class PizzaOrder extends Model
{
    protected $table = 'pizza_orders';
    protected $guarded = ['id'];

    function pizza() {
        return $this->belongsTo(Pizza::class, 'pizza_id');
    }

    function extras() {
        return $this->hasManyThrough(Ingredient::class, PizzaOrderHasIngredients::class, 'pizza_order_id', 'id', 'id', 'ingredient_id');
    }
}
