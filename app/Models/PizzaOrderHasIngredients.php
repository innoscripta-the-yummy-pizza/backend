<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class PizzaOrderHasIngredients extends Model
{
    protected $table = 'pizza_orders_have_ingredients';
    protected $guarded = ['id'];
}
