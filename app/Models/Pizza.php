<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    protected $table = 'pizzas';
    protected $guarded = ['id'];
    protected $appends = ['image_url'];

    /**
     * Append attributes
     */
    public function getImageUrlAttribute()
    {
        $image = isset($this->attributes['image']) ? $this->attributes['image'] : "";
        return config('app.resources_url') . "/pizza/$image";
    }
}
