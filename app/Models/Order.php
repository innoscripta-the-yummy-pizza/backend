<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $guarded = ['id'];

    function pizza_orders() {
        return $this->hasMany(PizzaOrder::class, 'order_id', 'id');
    }
}
