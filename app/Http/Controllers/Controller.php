<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Aws\S3\S3Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function saveMultipart ($path, $extension, $s3Directory) {
        try {
            // Prepare filename
            $fileName = (string)(date("YmdHis")) . (string)(rand(1,9)) .'.'. $extension;
            $filepath = "$s3Directory/$fileName";

            // Prepare the upload parameters.
            $s3 = S3Client::factory(config('app.s3'));
            $s3->putObject([
                'Bucket' => config('app.s3_bucket'),
                'Key'    => $filepath,
                'SourceFile' => $path,
                'ContentType' => "image/$extension",
            ]);

            return $fileName;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            return NULL;
        }
    }

    public function deleteFile($path)
    {
        try {
            $s3 = S3Client::factory(config('app.s3'));
            $s3->deleteObject(array(
                'Bucket' => config('app.s3_bucket'),
                'Key' => $path,
            ));
        } catch (Exception $e) {
            logger('Error creating item: ' . $e);
        }
    }
}
