<?php

namespace Api\Version1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Models\Customer;

class ProfileController extends Controller
{
    /**
     * @api {get} /api/v1/profile My Profile
     * @apiName My Profile
     * @apiDescription Current profile.
     * @apiGroup Profile
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Profile retrieved succesfully.",
     *   "data": {
     *   "id": 2,
     *   "username": "mesabg",
     *   "first_name": "Moisés",
     *   "last_name": "Berenguer",
     *   "email": "moises.berenguer@gmail.com",
     *   "profile_picture": null,
     *   "profile_picture_url": null
     * },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function myProfile(Request $request)
    {
        try {
            $me = auth()->user()->only([
                'id',
                'username',
                'first_name',
                'last_name',
                'email',
                'profile_picture',
                'profile_picture_url',
            ]);

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.profile.my_profile.success'),
                "data" =>  $me,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {patch} /api/v1/profile Update My Profile
     * @apiName Update My Profile
     * @apiDescription Updates my profile information. NOTE: All the fields are optional as long as this is a partial update.
     * @apiGroup Profile
     *
     * @apiParam {String} username Username
     * @apiParam {String} first_name First Name
     * @apiParam {String} last_name Last Name
     *
     * @apiExample {json} Example Usage:
     * {
     *   "username": "mesabg",
     *   "first_name": "José",
     *   "last_name": "Ramón"
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Profile updated successfully.",
     *   "data": {
     *   "id": 2,
     *   "username": "mesabg",
     *   "first_name": "José",
     *   "last_name": "Ramón",
     *   "email": "moises.berenguer@gmail.com",
     *   "profile_picture": null,
     *   "profile_picture_url": null
     * },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function updateMyProfile(Request $request)
    {
        try {
            $parameters = array_merge(request()->route()->parameters(), request()->all());
            $validator = \Validator::make($parameters, [
                'username' => ['string', 'max:255'],
                'first_name' => ['string', 'max:255'],
                'last_name' => ['string', 'max:255'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.profile.update_my_profile.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }

            \DB::beginTransaction();
            $customer = auth()->user();
            if (isset($request->username) && $request->username != '' && $request->username != $customer->username) {
                $usernameCustomer = Customer::where([ 'username' => $request->username ])->first();
                if (isset($usernameCustomer)) {
                    return response()->restResponse([
                        "status" => __('rest.success'),
                        "statusCode" => 400,
                        "message" => __('rest.profile.update_my_profile.username_in_use'),
                        "data" =>  NULL,
                        "errors" => NULL,
                    ]);
                }
                $customer->username = $request->username;
            }
            if (isset($request->first_name) && $request->first_name != '' && $request->first_name != $customer->first_name) {
                $customer->first_name = $request->first_name;
            }
            if (isset($request->last_name) && $request->last_name != '' && $request->last_name != $customer->last_name) {
                $customer->last_name = $request->last_name;
            }
            $customer->save();
            \DB::commit();

            $customer = $customer->only([
                'id',
                'username',
                'first_name',
                'last_name',
                'email',
                'profile_picture',
                'profile_picture_url',
            ]);

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.profile.update_my_profile.success'),
                "data" =>  $customer,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {get} /api/v1/profile/delivery My Delivery Info
     * @apiName My Delivery Info
     * @apiDescription My current delivery info.
     * @apiGroup Profile
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "My delivery info retrieved successfully.",
     *   "data": {
     *   "id": 2,
     *   "address_line_1": null,
     *   "address_line_2": null,
     *   "city": null,
     *   "zip": null,
     *   "phone": null,
     *   "secondary_phone": null
     * },
     *   "errors": null
     * } 
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function myDeliveryInfo(Request $request)
    {
        try {
            $myDeliveryInfo = auth()->user()->only([
                'id',
                'address_line_1',
                'address_line_2',
                'city',
                'zip',
                'phone',
                'secondary_phone',
            ]);
            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.profile.my_delivery_info.success'),
                "data" =>  $myDeliveryInfo,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {patch} /api/v1/profile/delivery Update My Delivery Info
     * @apiName Update My Delivery Info
     * @apiDescription Updates my current delivery information. NOTE: All the fields are optional as long as this is a partial update.
     * @apiGroup Profile
     *
     * @apiParam {String} address_line_1 Address Line 1
     * @apiParam {String} address_line_2 Address Line 2
     * @apiParam {String} city City
     * @apiParam {String} zip Zip
     * @apiParam {String} phone Phone
     * @apiParam {String} secondary_phone Secondary Phone
     *
     * @apiExample {json} Example Usage:
     * {
     *   "address_line_1": "Some address",
     *   "address_line_2": "Some address 2",
     *   "city": "Caracas",
     *   "zip": "2020",
     *   "phone": "04127589860",
     *   "secondary_phone": "04127845896"
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "My delivery info was updated successfully",
     *   "data": {
     *   "id": 2,
     *   "address_line_1": "Some address",
     *   "address_line_2": "Some address 2",
     *   "city": "Caracas",
     *   "zip": "2020",
     *   "phone": "04127589860",
     *   "secondary_phone": "04127845896"
     * },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function updateMyDeliveryInfo (Request $request)
    {
        try {
            $parameters = array_merge(request()->route()->parameters(), request()->all());
            $validator = \Validator::make($parameters, [
                'address_line_1' => ['string', 'max:255'],
                'address_line_2' => ['string', 'max:255'],
                'city' => ['string', 'max:255'],
                'zip' => ['string', 'max:255'],
                'phone' => ['string', 'max:255'],
                'secondary_phone' => ['string', 'max:255'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.profile.update_my_delivery_info.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }

            \DB::beginTransaction();
            $customer = auth()->user();
            if (isset($request->address_line_1) && $request->address_line_1 != '' && $request->address_line_1 != $customer->address_line_1) {
                $customer->address_line_1 = $request->address_line_1;
            }
            if (isset($request->address_line_2) && $request->address_line_2 != '' && $request->address_line_2 != $customer->address_line_2) {
                $customer->address_line_2 = $request->address_line_2;
            }
            if (isset($request->city) && $request->city != '' && $request->city != $customer->city) {
                $customer->city = $request->city;
            }
            if (isset($request->zip) && $request->zip != '' && $request->zip != $customer->zip) {
                $customer->zip = $request->zip;
            }
            if (isset($request->phone) && $request->phone != '' && $request->phone != $customer->phone) {
                $customer->phone = $request->phone;
            }
            if (isset($request->secondary_phone) && $request->secondary_phone != '' && $request->secondary_phone != $customer->secondary_phone) {
                $customer->secondary_phone = $request->secondary_phone;
            }
            $customer->save();
            \DB::commit();

            $myDeliveryInfo = $customer->only([
                'id',
                'address_line_1',
                'address_line_2',
                'city',
                'zip',
                'phone',
                'secondary_phone',
            ]);

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.profile.update_my_delivery_info.success'),
                "data" =>  $myDeliveryInfo,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {put} /api/v1/profile/update-password Update Password
     * @apiName Update Password
     * @apiDescription Updates my password.
     * @apiGroup Profile
     *
     * @apiParam {String} password Password
     * @apiParam {String} password_confirmation Password Confirmation
     *
     * @apiExample {json} Example Usage:
     * {
     *   "password": "789456123",
     *   "password_confirmation": "789456123"
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Password updated succesfully.",
     *   "data": {
     *       "id": 2,
     *       "username": "mesabg",
     *       "first_name": "José",
     *       "last_name": "Ramón",
     *       "email": "moises.berenguer@gmail.com",
     *       "profile_picture": null,
     *       "profile_picture_url": null
     * },
     *    "errors": null
}
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */

    public function updatePassword (Request $request)
    {
        try {
            $parameters = array_merge(request()->route()->parameters(), request()->all());
            $validator = \Validator::make($parameters, [
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.profile.update_password.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }

            \DB::beginTransaction();
            $customer = auth()->user();
            $customer->password = bcrypt($request->password);
            $customer->save();
            \DB::commit();

            $customer = $customer->only([
                'id',
                'username',
                'first_name',
                'last_name',
                'email',
                'profile_picture',
                'profile_picture_url',
            ]);

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.profile.update_password.success'),
                "data" =>  $customer,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {post} /api/v1/profile/update-profile-picture Update Profile Picture
     * @apiName Update Profile Picture
     * @apiDescription Change my profile picture icon.
     * @apiGroup Profile
     *
     * @apiParam {String} file Profile picture. This parameter must be a multipart binary file.
     * 
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Profile picture updated successfully.",
     *   "data": {
     *       "id": 2,
     *       "username": "mesabg",
     *       "first_name": "José",
     *       "last_name": "Ramón",
     *       "email": "moises.berenguer@gmail.com",
     *       "profile_picture": "202005201727397.jpeg",
     *       "profile_picture_url": "https://innoscripta-resources-dev.mesabg.com/profile/202005201727397.jpeg"
     * },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function updateProfilePicture(Request $request)
    {
        try {
            $parameters = array_merge(
                request()->route()->parameters(),
                request()->query(),
                request()->all()
            );
            $validator = \Validator::make($parameters, [
                'file' => ['required', 'file'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.profile.update_profile_picture.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }

            $path = request()->file('file')->path();
            $extension = request()->file('file')->extension();

            // Perform database save | delete old picture from the bucket and upload the new one
            \DB::beginTransaction();
            $customer = auth()->user();
            if (isset($customer->profile_picture)){
                $this->deleteFile("profile/$customer->profile_picture");
            }
            $customer->profile_picture = $this->saveMultipart($path, $extension, 'profile');
            $customer->save();
            \DB::commit();

            $customer = $customer->only([
                'id',
                'username',
                'first_name',
                'last_name',
                'email',
                'profile_picture',
                'profile_picture_url',
            ]);

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.profile.update_profile_picture.success'),
                "data" =>  $customer,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }
}
