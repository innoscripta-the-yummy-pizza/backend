<?php

namespace Api\Version1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Models\Pizza;
use Models\Ingredient;

class MenuController extends Controller
{
    /**
     * @api {get} /api/v1/menu Retrieve pizzas
     * @apiName Retrieve pizzas
     * @apiDescription Get all the menu.
     * @apiGroup Menu
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "All pizzas retrieve successfully.",
     *   "data": [
     * {
     *   "id": 1,
     *   "name": "Bacon Corn",
     *   "image": "BaconCorn.jpg",
     *   "description": "Tender corn and bacon.",
     *   "price": 19.99,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "image_url": "https://innoscripta-resources-dev.mesabg.com/pizza/BaconCorn.jpg"
     * },
     * {
     *   "id": 2,
     *   "name": "Bacon Corn 2",
     *   "image": "BaconCorn2.jpg",
     *   "description": "Tender corn and extra bacon.",
     *   "price": 21.99,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "image_url": "https://innoscripta-resources-dev.mesabg.com/pizza/BaconCorn2.jpg"
     * },
     * {
     *   "id": 3,
     *   "name": "Chicken And Mustard Honey",
     *   "image": "ChickenAndMustardHoney.jpg",
     *   "description": "Cheese and honey mustard chicken.",
     *   "price": 24.99,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "image_url": "https://innoscripta-resources-dev.mesabg.com/pizza/ChickenAndMustardHoney.jpg"
     * },
     * ...
     * ],
     *
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */

    public function retrievePizzas(Request $request)
    {
        try {
            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.menu.retrieve_pizzas.success'),
                "data" =>  Pizza::all(),
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {get} /api/v1/menu/:id Single pizza
     * @apiName Single pizza
     * @apiDescription Get single pizza detail.
     * @apiGroup Menu
     *
     * @apiParam {String} id Pizza id.
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Pizza details retrieved successfully.",
     *   "data": {
     *   "id": 1,
     *   "name": "Bacon Corn",
     *   "image": "BaconCorn.jpg",
     *   "description": "Tender corn and bacon.",
     *   "price": 19.99,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "image_url": "https://innoscripta-resources-dev.mesabg.com/pizza/BaconCorn.jpg"
     * },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function singlePizza(Request $request)
    {
        try {
            $parameters = array_merge(request()->route()->parameters(), request()->all());
            $validator = \Validator::make($parameters, [
                'pizza_id' => ['required', 'string', 'max:255', 'exists:pizzas,id'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.menu.single_pizza.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }

            $pizza = Pizza::find($request->pizza_id);
            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.menu.single_pizza.success'),
                "data" =>  $pizza,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {get} /api/v1/menu/extras Retrieve extras
     * @apiName Retrieve extras
     * @apiDescription Retrieve all possible extra ingredients.
     * @apiGroup Menu
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Extras retrieved successfully.",
     *   "data": [
     * {
     *   "id": 1,
     *   "name": "Corn",
     *   "price": 5.21,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z"
     * },
     * {
     *   "id": 2,
     *   "name": "Peppers",
     *   "price": 6.21,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z"
     * },
     * ...
     * ],
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function retrieveExtras(Request $request)
    {
        try {
            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.menu.retrieve_extras.success'),
                "data" =>  Ingredient::all(),
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }
}
