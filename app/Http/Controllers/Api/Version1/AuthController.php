<?php

namespace Api\Version1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Models\Customer;
use App\Mail\Welcome as WelcomeMail;
use App\Mail\RecoverPassword as RecoverPasswordMail;
use App\Mail\ActivateAccount as ActivateAccountMail;
use App\Mail\PasswordChanged as PasswordChangedMail;
use App\Mail\AccountNowActive as AccountNowActiveMail;

class AuthController extends Controller
{

    /**
     * @api {post} /api/v1/auth/sign-up Register
     * @apiName Register
     * @apiDescription Creates a new customer and returns the session started as well. So it works as register auto login. Also sends welcome mailing.
     * @apiGroup Auth
     *
     * @apiParam {String} username Customer username identifier.
     * @apiParam {String} email Customer personal email.
     * @apiParam {String} first_name Customer first name.
     * @apiParam {String} last_name Customer last name.
     * @apiParam {String} password Customer password.
     * @apiParam {String} password_confirmation Customer password confirmation.
     *
     * @apiExample {json} Example Usage:
     * {
     *   "username": "mesabg",
     *   "email": "moises.berenguer@gmail.com",
     *   "first_name": "Moisés",
     *   "last_name": "Berenguer",
     *   "password": "somepasswordfortest",
     *   "password_confirmation": "somepasswordfortest"
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 201,
     *   "message": "Customer was created successfully.",
     *   "data": {
     *     "customer": {
     *       "username": "mesabg",
     *       "email": "moises.berenguer@gmail.com",
     *       "first_name": "Moisés",
     *       "last_name": "Berenguer",
     *       "updated_at": "2020-05-18T13:45:26.000000Z",
     *       "created_at": "2020-05-18T13:45:26.000000Z",
     *       "id": 2
     *     },
     *     "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMTc1OTkwNTFjYjAzMzM4ODNjMWQ1ZGVhNDA0NjY4NWVhNTA5MTQ1ZGRmNWJlYmZlMjQwMjJhYTVmMzg3OGY0NmJhZTA4MTAwM2RjOTAwZWEiLCJpYXQiOjE1ODk4MDk1MjYsIm5iZiI6MTU4OTgwOTUyNiwiZXhwIjoxNjIxMzQ1NTI2LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.g2zhKeU48j3wrz5uVa9DhQPKEcDRbETJEhfhXcuE760xwsw45qmAEtc8a_UltiMRIF4wFNvKPQdL19DMzDR7t4q1IbPh-kfoN5Dk6ATKOTyuGH7WGh1AQtn64cbKklYOa5PZgJ7ARyajbuZZICe9odL2ji3OYdszhjpx-d_RQ0_fq6HdVIQbaNdoyXtciLbMCKQRN8z9Fz0SDJvXI0WKyUZTvShOiioEV3ITu6wLaULZtwl2AP7SEv0MTCuE_-1eaBeNR-tmH6oh1K3x5B_3FyhbE0vRWFBjDTM26Dcm6WgzF2cHJc0tIIMDH5RkIW4mRMiflBACygO_a7NFrfk76n18zOJLYvR20kvqbOh5zPwR0-6oGAuRRTQIwWF1XoWPxr8DCHZAeAzJTbdK8lohV792HS0akH0uNbdA7CEWDOUQWtwxfjbfxKmvFs-SXQJblKpR2VnbhhiCC712JjMq6jISNG2rV_5HB9QD1MYKPRf5eHbGqRl4vGCzCNk50ENweugtI1mw2_5-zpB28NCnDRRHp7fzmN5-Baq4znesXI86USJJ_vYk9S1sMVMvOfOW9DJNPvthXOKYi0L14E0S_TEKMo8jbr3Hjw9SDh9IJAvc-pF3T_4UoV8kIDAcylwtncD9xvfSZiXWkzTTGIukwUWYQxdI32j3-tqxKaMgjXA",
     *   },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function signUp(Request $request)
    {
        try {
            $parameters = array_merge(request()->route()->parameters(), request()->all());
            $validator = \Validator::make($parameters, [
                // Basic information
                'username' => ['required', 'string', 'max:255', 'unique:customers,username'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:customers,email'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => [],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.register.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }
            $validatedData = $validator->getData();
            $validatedData['password'] = bcrypt($validatedData['password']);

            // Save customer into the database | generates session token | send welcome mail
            \DB::beginTransaction();
            $key = md5(rand());
            $customer = Customer::create($validatedData);
            $customer->key = $key;
            $customer->save();
            $accessToken = $customer->createToken('authToken')->accessToken;
            \Mail::to($customer->email)->send(new WelcomeMail([
                'id' => $customer->id,
                'name' => $customer->first_name . ' ' . $customer->last_name,
                'username' => $customer->username,
                'email' => $customer->email,
                'key' => $key,
            ]));
            \DB::commit();

            $customer = $customer->only([
                'id',
                'username',
                'first_name',
                'last_name',
                'email',
                'profile_picture',
                'profile_picture_url',
            ]);

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 201,
                "message" => __('rest.auth.register.success'),
                "data" => [
                    "customer" => $customer,
                    "access_token" => $accessToken,
                ],
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }


    /**
     * @api {post} /api/v1/auth/sign-in Login
     * @apiName Login
     * @apiDescription Creates a new session for the customer.
     * @apiGroup Auth
     *
     * @apiParam {String} email Customer personal email.
     * @apiParam {String} password Customer password.
     * @apiParam {String} [type=customer] Use admin for CMS login.
     *
     * @apiExample {json} Example Usage:
     * {
     *   "email": "moises.berenguer@gmail.com",
     *   "password": "somepasswordfortest"
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Login success.",
     *   "data": {
     *     "customer": {
     *       "id": 18,
     *       "username": "mesabg",
     *       "first_name": "Moisés",
     *       "last_name": "Berenguer",
     *       "email": "moises.berenguer@gmail.com",
     *       "address_line_1": null,
     *       "address_line_2": null,
     *       "city": null,
     *       "zip": null,
     *       "phone": null,
     *       "secondary_phone": null,
     *       "verified_at": null,
     *       "verified": 0,
     *       "created_at": "2020-05-19T01:08:10.000000Z",
     *       "updated_at": "2020-05-19T01:08:10.000000Z"
     *     },
     *     "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZmI4OGVmOWJkYTAwYmFkODgxMTk3OTFiZTczOGE2NjM4ODllYzhmNmE1NjZjZTVhODA1NTkwMjVmYjEwZmRjZTFhMzY2MDY3ZDRiYmY1ZjkiLCJpYXQiOjE1ODk4NTA3MDMsIm5iZiI6MTU4OTg1MDcwMywiZXhwIjoxNjIxMzg2NzAzLCJzdWIiOiIxOCIsInNjb3BlcyI6W119.oRLL3tW4UT2gCaJTChaiV6CXVzmCdI7NC422Ka_pvg6jy-TbLAbqBfqx6HZCKAHAZl4geumOAdDCl-TjPT-uWD6QHL_bYuVxKlgAD_oSazp6JulAkn3Jr7DSchDpb7_a40u5l_Wt3IH_4yR1a383u-xqAvbruEOm0ee_lXtRygx-KBaCsCgja3-0BeuiWE5hgn5cgr_CcUNcqEwrbkNbB7OJ2LybSCr36JzE8BFABMNrQi69LX_vBkY7g5yZFHEhsR6_4rPUpD0AQbaU4SJYyiwnmOWrfRfX5wS0VPHUDJiVjxruZiKUxjIM6d7Lu_bv6FSCsIeo7jwQFnOI91PWeXUXeQlh1pG8CTN4j11o5SajvvywyhdyajhvlcmGu4Y95qy4BZG45klqQXGZjfwlUqdGWpK3zeh53pDzNb3cMIEz7dNL4LHKXutdUTtMlXnUHSi-l_skCWV5FAIOkFDkvCyp2s4IwQ4YAtE055LcBdumFPVSVAPk9D9lwdsetjwJE-8hT8NySb_B_RJ3wqh_IU0cqXl-oNBCJZf3kBgUU8jE_bNTT-Hoh-MI9meb9gIqTPNdl-ytgFcPJMKQ2Ce_B9gOTCQ3MrLbX5nHBNaWQ6HPvSpatHyCqg24i8D9Mg3qN8TwR2YN9YiiLy96fPzG_toLW8uyEoaXhKX95d2F7hQ"
     *   },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function signIn(Request $request)
    {
        try {
            $parameters = array_merge(request()->route()->parameters(), request()->all());
            $validator = \Validator::make($parameters, [
                'type' => ['string', 'in:customer,admin'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'password' => ['required', 'string', 'min:8'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.login.error_data'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }

            // Validate the guard
            $type = $request->type;
            $guard = !isset($type) ? 'customer' : $type;

            // Login the user
            $credentials = $request->only('email', 'password');
            if(!\Auth::guard("web:$guard")->attempt($credentials)){
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.login.error_attempt'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            // Generate the token
            $customer = auth()->guard("web:$guard")->user();
            $accessToken = $customer->createToken('authToken')->accessToken;
            $customer = $customer->only([
                'id',
                'username',
                'first_name',
                'last_name',
                'email',
                'profile_picture',
                'profile_picture_url',
            ]);

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.auth.login.success'),
                "data" => [
                    "customer" => $customer,
                    "access_token" => $accessToken,
                ],
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }


    /**
     * @api {post} /api/v1/auth/forgot-password Forgot Password
     * @apiName Forgot Password
     * @apiDescription Sends forgot password mail with the link to reset.
     * @apiGroup Auth
     *
     * @apiParam {String} email Customer personal email.
     *
     * @apiExample {json} Example Usage:
     * {
     *   "email": "moises.berenguer@gmail.com"
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Forgot password email sent properly.",
     *   "data": null,
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function forgotPassword(Request $request)
    {
        try {
            $parameters = array_merge(request()->route()->parameters(), request()->all());
            $validator = \Validator::make($parameters, [
                'email' => ['required', 'string', 'email', 'max:255', 'exists:customers,email'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.forgot_password.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }

            // Update customer and send mail
            \DB::beginTransaction();
            $forgot_password_key = md5(rand());
            $customer = Customer::where([ 'email' => request()->email ])->first();
            $customer->forgot_password_key = $forgot_password_key;
            $customer->forgot_password_key_expiration = now()->addDays(1);
            $customer->save();
            \Mail::to($customer->email)->send(new RecoverPasswordMail([
                'id' => $customer->id,
                'name' => $customer->first_name . ' ' . $customer->last_name,
                'forgot_password_key' => $forgot_password_key,
            ]));
            \DB::commit();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.auth.forgot_password.success'),
                "data" => NULL,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {post} /api/v1/auth/recover-password/:id/:key  Recover-Password
     * @apiName Recover-Password
     * @apiDescription Update customer password if the key match and if it is not expired.
     * @apiGroup Auth
     *
     * @apiParam {String} id Customer id.
     * @apiParam {String} key Verification key id
     * @apiParam {String} password  Customer password.
     * @apiParam {String} password_confirmation Customer password confirmation.
     *
     * @apiExample {json} Example Usage:
     * {
     *   "password": "456789123",
	 *   "password_confirmation": "456789123"
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Password restored successfully.",
     *   "data": null,
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function recoverPassword(Request $request)
    {
        try {
            $parameters = array_merge(request()->route()->parameters(), request()->all());
            $validator = \Validator::make($parameters, [
                'id' => ['required', 'string', 'max:255', 'exists:customers,id'],
                'key' => ['required', 'string', 'max:255', 'exists:customers,forgot_password_key'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.recover_password.parameters_error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }

            // Update customer and send mail account activated
            \DB::beginTransaction();
            $customer = Customer::where([ 'id' => request()->id ])->first()->makeVisible(['forgot_password_key']);

            // Validate key and expiration time
            if ($customer->forgot_password_key != request()->key) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.recover_password.key_not_match'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            if (now() > $customer->forgot_password_key_expiration) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.recover_password.expired_key'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            $customer->password = bcrypt(request()->password);
            $customer->forgot_password_key = NULL;
            $customer->forgot_password_key_expiration = NULL;
            $customer->save();
            \Mail::to($customer->email)->send(new PasswordChangedMail([
                'name' => $customer->first_name . ' ' . $customer->last_name,
            ]));
            \DB::commit();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.auth.recover_password.success'),
                "data" => NULL,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }
    /**
     * @api {post} /api/v1/auth/resend-validation-link Resend Validation Link
     * @apiName Resend Validation Link
     * @apiDescription Resend mail for account activation.
     * @apiGroup Auth
     *
     * @apiParam {String} email Customer personal email.
     *
     * @apiExample {json} Example Usage:
     * {
     *   "email": "moises.berenguer@gmail.com"
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Validation link has been sent succesfully.",
     *   "data": null,
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */

    public function resendValidationLink(Request $request)
    {
        try {
            $parameters = array_merge(request()->route()->parameters(), request()->all());
            $validator = \Validator::make($parameters, [
                'email' => ['required', 'string', 'email', 'max:255', 'exists:customers,email'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.resend_validation_link.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }

            // Update customer and send mail
            \DB::beginTransaction();
            $key = md5(rand());
            $customer = auth()->user();

            if ($customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.success'),
                    "statusCode" => 200,
                    "message" => __('rest.auth.resend_validation_link.account_already_activated'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            $customer->key = $key;
            $customer->key_expiration = now()->addDays(1);
            $customer->save();
            \Mail::to($customer->email)->send(new ActivateAccountMail([
                'id' => $customer->id,
                'name' => $customer->first_name . ' ' . $customer->last_name,
                'username' => $customer->username,
                'email' => $customer->email,
                'key' => $key,
            ]));
            \DB::commit();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.auth.resend_validation_link.success'),
                "data" => NULL,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {post} /api/v1/auth/validate-account/:id/:key Validate Account
     * @apiName Validate Account
     * @apiDescription Make the account valid.
     * @apiGroup Auth
     *
     * @apiParam {String} id Customer id.
     * @apiParam {String} key Verification key id
     * @apiParam {String} password  Customer password.
     * @apiParam {String} password_confirmation Customer password confirmation.
     *
     * @apiExample {json} Example Usage:
     * {
     *   "password": "456789123",
	 *   "password_confirmation": "456789123"
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Account successfully verified.",
     *   "data": null,
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function validateAccount(Request $request)
    {
        try {
            $parameters = array_merge(request()->route()->parameters(), request()->all());
            $validator = \Validator::make($parameters, [
                'id' => ['required', 'string', 'max:255', 'exists:customers,id'],
                'key' => ['required', 'string', 'max:255', 'exists:customers,key'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.validate_account.parameters_error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }

            // Update customer and send mail account activated
            \DB::beginTransaction();
            $customer = Customer::where([ 'id' => request()->id ])->first()->makeVisible(['key']);

            if ($customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.success'),
                    "statusCode" => 200,
                    "message" => __('rest.auth.validate_account.already_verified'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            // Validate key and expiration time
            if ($customer->key != request()->key) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.validate_account.key_not_match'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            $now = now();
            if ($now > $customer->key_expiration) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.auth.validate_account.expired_key'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            $customer->password = bcrypt(request()->password);
            $customer->key = NULL;
            $customer->key_expiration = NULL;
            $customer->verified = true;
            $customer->verified_at = $now;
            $customer->save();
            \Mail::to($customer->email)->send(new AccountNowActiveMail([
                'name' => $customer->first_name . ' ' . $customer->last_name,
            ]));
            \DB::commit();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.auth.validate_account.success'),
                "data" => NULL,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }
}
