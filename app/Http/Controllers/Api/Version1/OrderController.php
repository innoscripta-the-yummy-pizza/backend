<?php

namespace Api\Version1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Models\Pizza;
use Models\Ingredient;
use Models\Customer;
use Models\Order;
use Models\PizzaOrder;
use Models\PizzaOrderHasIngredients;
use Stripe\Stripe;
use Stripe\Customer as CustomerStripe;
use Stripe\Charge as ChargeStripe;

class OrderController extends Controller
{
    /**
     * @api {post} /api/v1/order Create order
     * @apiName Create order
     * @apiDescription Creates an order fot the customer.
     * @apiGroup Order
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Order created successfully.",
     *   "data": {
     *   "customer_id": 2,
     *   "status": "created",
     *   "total_price": 0,
     *   "estimated_delivery_time": 15,
     *   "updated_at": "2020-05-20T17:20:16.000000Z",
     *   "created_at": "2020-05-20T17:20:16.000000Z",
     *   "id": 1
     * },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function createOrder(Request $request)
    {
        try {
            $customer = auth()->user();

            // Validations
            if (!$customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.unverified_user'),
                    "data" =>  NULL,
                    "errors" => NULL,
                ]);
            }

            \DB::beginTransaction();
            $order = $customer->current_order()->first();
            if (isset($order)) {
                return response()->restResponse([
                    "status" => __('rest.success'),
                    "statusCode" => 200,
                    "message" => __('rest.order.create_order.order_already_exist'),
                    "data" =>  $order,
                    "errors" => NULL,
                ]);
            }
            $order = Order::create([
                'customer_id' => $customer->id,
                'status' => 'created',
                'total_price' => 0,
                'estimated_delivery_time' => 15,
            ]);
            \DB::commit();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.order.create_order.success'),
                "data" =>  $order,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {get} /api/v1/order Retrieve Order
     * @apiName Retrieve Order
     * @apiDescription Retrieve current active order for the customer.
     * @apiGroup Order
     *
     * @apiSuccessExample Success:
    * {
    *   "status": "success",
    *   "statusCode": 200,
    *   "message": "Current order retrieved successfully.",
    *   "data": {
    *   "id": 1,
    *   "customer_id": 2,
    *   "status": "created",
    *   "total_price": 0,
    *   "estimated_delivery_time": 15,
    *   "delivery_cost": 15,
    *   "paid": 0,
    *   "created_at": "2020-05-20T17:20:16.000000Z",
    *   "updated_at": "2020-05-20T17:20:16.000000Z",
    *   "pizza_orders": []
    * },
    *   "errors": null
    * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function retrieveCurrentOrder(Request $request)
    {
        try {
            $customer = auth()->user();

            // Validations
            if (!$customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.unverified_user'),
                    "data" =>  NULL,
                    "errors" => NULL,
                ]);
            }

            $customer = auth()->user();
            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.order.retrieve_current_order.success'),
                "data" =>  $customer->current_order()->first(),
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {post} /api/v1/order/pizza/:pizza_id Add Pizza To Current Order
     * @apiName Add Pizza To Current Order
     * @apiDescription Add pizza with custom configuration to my current open order.
     * @apiGroup Order
     *
     * @apiParam {String} pizza_id  Pizza id.
     * @apiParam {String} extras Extras
     * @apiParam {String} quantity Quantity
     *
     * @apiExample {json} Example Usage:
     * {
     *   "extras": [1, 2, 3, 4],
     *   "quantity": 5
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Pizza added to the current order successfully",
     *   "data": {
     *   "id": 1,
     *   "customer_id": 2,
     *   "status": "created",
     *   "total_price": 183.05,
     *   "estimated_delivery_time": 15,
     *   "delivery_cost": 15,
     *   "paid": 0,
     *   "created_at": "2020-05-20T17:20:16.000000Z",
     *   "updated_at": "2020-05-20T17:21:52.000000Z",
     *   "pizza_orders": [
     * {
     *   "id": 1,
     *   "quantity": 5,
     *   "pizza_id": 1,
     *   "order_id": 1,
     *   "created_at": "2020-05-20T17:21:52.000000Z",
     *   "updated_at": "2020-05-20T17:21:52.000000Z",
     *   "pizza": {
     *   "id": 1,
     *   "name": "Bacon Corn",
     *   "image": "BaconCorn.jpg",
     *   "description": "Tender corn and bacon.",
     *   "price": 19.99,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "image_url": "https://innoscripta-resources-dev.mesabg.com/pizza/BaconCorn.jpg"
     * },
     *   "extras": [
     * {
     *   "id": 1,
     *   "name": "Corn",
     *   "price": 5.21,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "laravel_through_key": 1
     * },
     * ...
     * ]
     * }
     * ]
     * },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function addPizzaToCurrentOrder(Request $request)
    {
        try {
            $parameters = array_merge(
                request()->route()->parameters(),
                request()->query(),
                request()->all()
            );
            $validator = \Validator::make($parameters, [
                'pizza_id' => ['required', 'string', 'max:255', 'exists:pizzas,id'],
                'extras' => ['required', 'array', 'exists:ingredients,id'],
                'quantity' => ['required', 'integer'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.add_pizza_to_current_order.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }
            $customer = auth()->user();

            // Validations
            if (!$customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.unverified_user'),
                    "data" =>  NULL,
                    "errors" => NULL,
                ]);
            }

            \DB::beginTransaction();
            $order = $customer->current_order()->first();
            if (!isset($order)) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.no_active_order'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            $total_price = 0;
            $pizza = Pizza::find($request->pizza_id);
            $total_price += $pizza->price;
            $pizzaOrder = PizzaOrder::create([
                'pizza_id' => $pizza->id,
                'order_id' => $order->id,
                'quantity' => $request->quantity,
            ]);
            foreach($request->extras as $ingredient_id) {
                $ingredient = Ingredient::find($ingredient_id);
                PizzaOrderHasIngredients::create([
                    'pizza_order_id' => $pizzaOrder->id,
                    'ingredient_id' => $ingredient->id,
                ]);
                $total_price += $ingredient->price;
            }
            $order->total_price += $total_price * $pizzaOrder->quantity;
            $order->save();
            $order = $customer->current_order()->first();
            \DB::commit();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.order.add_pizza_to_current_order.success'),
                "data" =>  $order,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {put} /api/v1/order/pizza/:pizza_order_id Update Pizza From Current Order
     * @apiName Update Pizza From Current Order
     * @apiDescription Update pizza on current order.
     * @apiGroup Order
     *
     * @apiParam {String} pizza_order_id  Pizza  order id
     * @apiParam {String} pizza_id Pizza id
     * @apiParam {String} extras Extras
     * @apiParam {String} quantity Quantity
     *
     * @apiExample {json} Example Usage:
     * {
     *   "pizza_id": 4,
     *   "extras": [6, 7, 8],
     *   "quantity": 1
     * }
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Pizza updated successfully on the current order.",
     *   "data": {
     *   "id": 1,
     *   "customer_id": 2,
     *   "status": "created",
     *   "total_price": 36.52,
     *   "estimated_delivery_time": 15,
     *   "delivery_cost": 15,
     *   "paid": 0,
     *   "created_at": "2020-05-20T17:20:16.000000Z",
     *   "updated_at": "2020-05-20T17:22:40.000000Z",
     *   "pizza_orders": [
     * {
     *   "id": 1,
     *   "quantity": 1,
     *   "pizza_id": 4,
     *   "order_id": 1,
     *   "created_at": "2020-05-20T17:21:52.000000Z",
     *   "updated_at": "2020-05-20T17:22:40.000000Z",
     *   "pizza": {
     *   "id": 4,
     *   "name": "Chicken BBQ",
     *   "image": "ChickenBBQ.jpg",
     *   "description": "Chicken in BBQ sauce.",
     *   "price": 24.99,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "image_url": "https://innoscripta-resources-dev.mesabg.com/pizza/ChickenBBQ.jpg"
     * },
     *   "extras": [
     * {
     *   "id": 6,
     *   "name": "Onion",
     *   "price": 1.55,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "laravel_through_key": 1
     * },
     * ...
     * ]
     * }
     * ]
     * },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function updatePizzaFromCurrentOrder(Request $request)
    {
        try {
            $parameters = array_merge(
                request()->route()->parameters(),
                request()->query(),
                request()->all()
            );
            $validator = \Validator::make($parameters, [
                'pizza_order_id' => ['required', 'string', 'max:255', 'exists:pizza_orders,id'],
                'pizza_id' => ['required', 'integer', 'exists:pizzas,id'],
                'extras' => ['required', 'array', 'exists:ingredients,id'],
                'quantity' => ['required', 'integer'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.update_pizza_from_current_order.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }
            $customer = auth()->user();

            // Validations
            if (!$customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.unverified_user'),
                    "data" =>  NULL,
                    "errors" => NULL,
                ]);
            }

            \DB::beginTransaction();
            $order = $customer->current_order()->first();
            if (!isset($order)) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.no_active_order'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            // Minus current configuration
            $total_price = 0;
            $pizzaOrder = $order->pizza_orders()
                ->where([ 'id' => $request->pizza_order_id ])
                ->first();
            if (!isset($pizzaOrder)) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.update_pizza_from_current_order.no_pizza_order_found'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }
            $pizza = $pizzaOrder->pizza()->first();
            $extras = $pizzaOrder->extras()->get();
            $total_price += $pizza->price;
            foreach($extras as $ingredient) {
                PizzaOrderHasIngredients::where([
                    'pizza_order_id' => $pizzaOrder->id,
                    'ingredient_id' => $ingredient->id,
                ])->delete();
                $total_price += $ingredient->price;
            }
            $order->total_price -= $total_price * $pizzaOrder->quantity;
            $order->save();

            // Plus new configuration
            $total_price = 0;
            $pizza = Pizza::find($request->pizza_id);
            $total_price += $pizza->price;
            $pizzaOrder->pizza_id = $pizza->id;
            $pizzaOrder->quantity = $request->quantity;
            $pizzaOrder->save();
            foreach($request->extras as $ingredient_id) {
                $ingredient = Ingredient::find($ingredient_id);
                PizzaOrderHasIngredients::create([
                    'pizza_order_id' => $pizzaOrder->id,
                    'ingredient_id' => $ingredient->id,
                ]);
                $total_price += $ingredient->price;
            }
            $order->total_price += $total_price * $pizzaOrder->quantity;
            $order->save();

            $order = $customer->current_order()->first();
            \DB::commit();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.order.update_pizza_from_current_order.success'),
                "data" =>  $order,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {del} /api/v1/order/pizza/:pizza_order_id Delete Pizza From Current Order
     * @apiName Delete Pizza From Current Order
     * @apiDescription Removes a pizza order from current order.
     * @apiGroup Order
     *
     * @apiParam {String} pizza_order_id Pizza order id.
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Pizza deleted from the current order.",
     *   "data": {
     *   "id": 1,
     *   "customer_id": 2,
     *   "status": "created",
     *   "total_price": 0,
     *   "estimated_delivery_time": 15,
     *   "delivery_cost": 15,
     *   "paid": 0,
     *   "created_at": "2020-05-20T17:20:16.000000Z",
     *   "updated_at": "2020-05-20T17:23:08.000000Z",
     *   "pizza_orders": []
     * },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function deletePizzaFromCurrentOrder(Request $request)
    {
        try {
            $parameters = array_merge(
                request()->route()->parameters(),
                request()->query(),
                request()->all()
            );
            $validator = \Validator::make($parameters, [
                'pizza_order_id' => ['required', 'string', 'max:255', 'exists:pizza_orders,id'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.delete_pizza_from_current_order.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }
            $customer = auth()->user();

            // Validations
            if (!$customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.unverified_user'),
                    "data" =>  NULL,
                    "errors" => NULL,
                ]);
            }

            \DB::beginTransaction();
            $order = $customer->current_order()->first();
            if (!isset($order)) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.no_active_order'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            // Minus current configuration
            $total_price = 0;
            $pizzaOrder = $order->pizza_orders()
                ->where([ 'id' => $request->pizza_order_id ])
                ->first();
            if (!isset($pizzaOrder)) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.delete_pizza_from_current_order.no_pizza_order_found'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }
            $pizza = $pizzaOrder->pizza()->first();
            $extras = $pizzaOrder->extras()->get();
            $total_price += $pizza->price;
            foreach($extras as $ingredient) {
                PizzaOrderHasIngredients::where([
                    'pizza_order_id' => $pizzaOrder->id,
                    'ingredient_id' => $ingredient->id,
                ])->delete();
                $total_price += $ingredient->price;
            }
            $order->total_price -= $total_price * $pizzaOrder->quantity;
            $order->save();
            $pizzaOrder->delete();

            $order = $customer->current_order()->first();
            \DB::commit();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.order.delete_pizza_from_current_order.success'),
                "data" =>  $order,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * 
     */
    public function placeOrder(Request $request)
    {
        try {
            $parameters = array_merge(
                request()->route()->parameters(),
                request()->query(),
                request()->all()
            );
            $validator = \Validator::make($parameters, [
                'stripeEmail' => ['required', 'string', 'max:255', 'email'],
                'stripeToken' => ['required', 'string'],
                'currency' => ['required', 'in:usd,eur'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.place_order.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }
            $customer = auth()->user();

            // Validations
            if (!$customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.unverified_user'),
                    "data" =>  NULL,
                    "errors" => NULL,
                ]);
            }

            \DB::beginTransaction();
            // Create charge in Stripe
            // REMEMBER: SUM DELIVERY COST
            $order = $customer->current_order()->first();
            if (!isset($order)) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.no_active_order'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            if ($order->paid) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.place_order.order_already_paid'),
                    "data" => NULL,
                    "errors" => NULL,
                ]);
            }

            Stripe::setApiKey(config('services.stripe.secret'));
            if (!isset($customer->stripe_id)) {
                $customerStripe = CustomerStripe::create(array(
                    'email' => $request->stripeEmail,
                    'source'  => $request->stripeToken
                ));
                $customer->stripe_id = $customerStripe->id;
                $customer->save();
                logger($customerStripe);
            }
            $chargeStripe = ChargeStripe::create(array(
                'customer' => $customer->stripe_id,
                'amount'   => $order->total_price + $order->delivery_cost,
                'currency' => $request->currency
            ));
            logger($chargeStripe);

            /*$order->status = 'preparing';
            $order->paid = true;
            $order->save();*/
            \DB::commit();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.order.place_order.success'),
                "data" =>  NULL,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            \DB::rollBack();
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {get} /api/v1/order/actives Active Orders
     * @apiName Active Orders
     * @apiDescription History of active orders (preparing, delivery, delayed).
     * @apiGroup Order
     * 
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Active orders retrieved successfully.",
     *   "data": [
     * {
     *   "id": 1,
     *   "customer_id": 2,
     *   "status": "preparing",
     *   "total_price": 183.05,
     *   "estimated_delivery_time": 15,
     *   "delivery_cost": 15,
     *   "paid": 1,
     *   "created_at": "2020-05-20T17:20:16.000000Z",
     *   "updated_at": "2020-05-20T17:29:48.000000Z",
     *   "pizza_orders": [
     * {
     *   "id": 2,
     *   "quantity": 5,
     *   "pizza_id": 1,
     *   "order_id": 1,
     *   "created_at": "2020-05-20T17:29:48.000000Z",
     *   "updated_at": "2020-05-20T17:29:48.000000Z",
     *   "pizza": {
     *   "id": 1,
     *   "name": "Bacon Corn",
     *   "image": "BaconCorn.jpg",
     *   "description": "Tender corn and bacon.",
     *   "price": 19.99,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "image_url": "https://innoscripta-resources-dev.mesabg.com/pizza/BaconCorn.jpg"
     * },
     *   "extras": [
     * {
     *   "id": 1,
     *   "name": "Corn",
     *   "price": 5.21,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "laravel_through_key": 2
     * },
     * ...
     * ]
     * }
     * ]
     * }
     * ],
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function retrieveActiveOrders(Request $request)
    {
        try {
            $parameters = array_merge(
                request()->route()->parameters(),
                request()->query(),
                request()->all()
            );
            $validator = \Validator::make($parameters, [
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.retrieve_actives_orders.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }
            $customer = auth()->user();

            // Validations
            if (!$customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.unverified_user'),
                    "data" =>  NULL,
                    "errors" => NULL,
                ]);
            }

            // Current active orders
            $orders = $customer->active_orders()->get();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.order.retrieve_actives_orders.success'),
                "data" =>  $orders,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {get} /api/v1/order/history History Orders
     * @apiName History Orders
     * @apiDescription Orders in complete or canceled statuses.
     * @apiGroup Order
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,     
     *   "message": "History of orders retrieved successfully.",
     *   "data": [
     * {
     *   "id": 1,
     *   "customer_id": 2,
     *   "status": "completed",
     *   "total_price": 183.05,
     *   "estimated_delivery_time": 15,
     *   "delivery_cost": 15,
     *   "paid": 1,
     *   "created_at": "2020-05-20T17:20:16.000000Z",
     *   "updated_at": "2020-05-20T17:29:48.000000Z",
     *   "pizza_orders": [
     * {
     *   "id": 2,
     *   "quantity": 5,
     *   "pizza_id": 1,
     *   "order_id": 1,
     *   "created_at": "2020-05-20T17:29:48.000000Z",
     *   "updated_at": "2020-05-20T17:29:48.000000Z",
     *   "pizza": {
     *   "id": 1,
     *   "name": "Bacon Corn",
     *   "image": "BaconCorn.jpg",
     *   "description": "Tender corn and bacon.",
     *   "price": 19.99,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "image_url": "https://innoscripta-resources-dev.mesabg.com/pizza/BaconCorn.jpg"
     * },
     *   "extras": [
     * {
     *   "id": 1,
     *   "name": "Corn",
     *   "price": 5.21,
     *   "created_at": "2020-05-20T17:10:38.000000Z",
     *   "updated_at": "2020-05-20T17:10:38.000000Z",
     *   "laravel_through_key": 2
     * },
     * ...
     * ]
     * }
     * ]
     * }
     * ],
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function retrieveHistoryOrders(Request $request)
    {
        try {
            $parameters = array_merge(
                request()->route()->parameters(),
                request()->query(),
                request()->all()
            );
            $validator = \Validator::make($parameters, [
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.retrieve_history_orders.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }
            $customer = auth()->user();

            // Validations
            if (!$customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.unverified_user'),
                    "data" =>  NULL,
                    "errors" => NULL,
                ]);
            }

            // Current active orders
            $orders = $customer->history_orders()->get();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.order.retrieve_history_orders.success'),
                "data" =>  $orders,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }

    /**
     * @api {get} /api/v1/order/:order_id Single Order
     * @apiName Single Order
     * @apiDescription Single order detail.
     * @apiGroup Order
     *
     * @apiParam {String} order_id Order id
     *
     * @apiSuccessExample Success:
     * {
     *   "status": "success",
     *   "statusCode": 200,
     *   "message": "Order retrieved correctly.",
     *   "data": {
     *   "id": 1,
     *   "customer_id": 2,
     *   "status": "created",
     *   "total_price": 0,
     *   "estimated_delivery_time": 15,
     *   "delivery_cost": 15,
     *   "paid": 0,
     *   "created_at": "2020-05-20T17:20:16.000000Z",
     *   "updated_at": "2020-05-20T17:20:16.000000Z"
     * },
     *   "errors": null
     * }
     *
     * @apiSuccess {String} status Status of request.
     * @apiSuccess {String} statusCode Status code for request.
     * @apiSuccess {String} errors Errors (null or String).
     * @apiSuccess {String} message Message.
     * @apiSuccess {Object} data Data of request (null or Object).
     */
    public function singleOrder(Request $request)
    {
        try {
            $parameters = array_merge(
                request()->route()->parameters(),
                request()->query(),
                request()->all()
            );
            $validator = \Validator::make($parameters, [
                'order_id' => ['required', 'string', 'max:255', 'exists:orders,id'],
            ]);
            if ($validator->fails()) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.order.single_order.error'),
                    "data" => NULL,
                    "errors" => [
                        "all" => collect($validator->messages())->values()->collapse(),
                        "fields" => $validator->messages()
                    ],
                ]);
            }
            $customer = auth()->user();

            // Validations
            if (!$customer->verified) {
                return response()->restResponse([
                    "status" => __('rest.error'),
                    "statusCode" => 400,
                    "message" => __('rest.unverified_user'),
                    "data" =>  NULL,
                    "errors" => NULL,
                ]);
            }

            // Current active orders
            $order = $customer->orders()
                ->where([ 'id' => $request->order_id ])
                ->first();

            return response()->restResponse([
                "status" => __('rest.success'),
                "statusCode" => 200,
                "message" => __('rest.order.single_order.success'),
                "data" =>  $order,
                "errors" => NULL,
            ]);
        } catch(\Exception $exception) {
            logger($exception->getMessage());
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => httpStatusCode((int)$exception->getCode()),
                "message" => $exception->getMessage(),
                "data" => NULL,
                "errors" => NULL,
            ]);
        }
    }
}
