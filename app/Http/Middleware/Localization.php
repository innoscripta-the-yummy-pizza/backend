<?php

namespace App\Http\Middleware;

use Closure;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    /**
     * @apiDefine XLocalizationHeader
     * @apiHeader {String} [X-localization=en] Response language [en, es].
     */
    /**
     * @apiDefine XLocalizationHeaderExample
     * @apiHeaderExample {json} X-localization (Ex):
     * {
     *     "X-localization": "es"
     * }
     */
    public function handle($request, Closure $next)
    {
        $localization = ($request->hasHeader('X-localization')) ? $request->header('X-localization') : 'en';
        app()->setLocale($localization);
        return $next($request);
    }
}
